## Installation

Fisrt you need to install all the dependencies required.
```sh
pip install -r requirements.txt
```

## Broser GUIDE

 - Run the application by command
```sh
python main.py
```
 - Once the application is running you can access by brower on url [http://localhost:5000](http://localhost:5000). 
 - Paste the desired URL on input.


## API Client Guide

### Postman, Insomnia, etc...

 - Send the request containing a url at the entry endpoint "localhost:5000/input" 'POST'
 ```
{
  "url": "https://www.amazon.com.br/Monitor-LG-19-5-LED-Inclina%C3%A7%C3%A3o/dp/B084TKF88Q?ref_=Oct_s9_apbd_simh_hd_bw_bHpofBj&pf_rd_r=8K5ZXBJJMMW4H41A6HNW&pf_rd_p=4631c5e2-eb1e-5c28-b639-352dd869f2fe&pf_rd_s=merchandised-search-11&pf_rd_t=BROWSE&pf_rd_i=16339926011"
}
 ```
 - See the exit of your request at the endpoint "localhost:5000/output" 'GET'