import os
import crochet
crochet.setup()
from flask import Flask , render_template, jsonify, request, redirect, url_for
from scrapy import signals
from scrapy.crawler import CrawlerRunner
from scrapy.signalmanager import dispatcher
import time
from scraping.scraping.spiders.amazon_scraping import ReviewspiderSpider


app = Flask(__name__)

output_data = []
crawl_runner = CrawlerRunner()

@app.route('/')
def index():
    return render_template("index.html")


@app.route('/', methods=['POST'])
def submit():
    if request.method == 'POST':
        s = request.form['url']
        global baseURL
        baseURL = s
        if os.path.exists("<path_to_outputfile.json>"): 
            os.remove("<path_to_outputfile.json>")
        return redirect(url_for('output'))

@app.route('/input', methods=['POST'])
def input():
    if request.method == 'POST':
        json_data = request.get_json(force=True)
        url = json_data['url']
        global baseURL
        baseURL = url
        if os.path.exists("<path_to_outputfile.json>"): 
            os.remove("<path_to_outputfile.json>")
        return redirect(url_for('output'))

@app.route("/output")
def output():
    scrape_with_crochet(baseURL=baseURL)
    time.sleep(20)
    return jsonify(output_data)


@crochet.run_in_reactor
def scrape_with_crochet(baseURL):
    dispatcher.connect(_crawler_result, signal=signals.item_scraped)
    eventual = crawl_runner.crawl(ReviewspiderSpider, category = baseURL)
    return eventual
def _crawler_result(item, response, spider):
    output_data.append(dict(item))


if __name__== "__main__":
    app.run(debug=True)